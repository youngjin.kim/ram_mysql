DTS=$(date +%m%d%Y_%H%M)
#attemps to reinstall a fresh copy of mariadb 10 to the server in case it's broken
echo "PROGRESS: purging mariadb and mysql from server"
sudo aptitude purge mariadb-server-10.0 mariadb-server -y
echo "PROGRESS: keeping a copy of the /var/lib/mysql directory"
sudo mv /var/lib/mysql /home/emphanos/ram_mysql/mysql_backup_$DTS

echo "PROGRESS: setting the mysql root password to be the same as in ~/.my.cnf"
root_mysql_password=`cat ~/.my.cnf | grep password= | head -1 | cut -d '"' -f2`
echo "PROGRESS: Found mysql root password to be: $root_mysql_password"
sudo debconf-set-selections <<< "root mysql-server/root_password password $root_mysql_password"
sudo debconf-set-selections <<< "root mysql-server/root_password_again password $root_mysql_password"

echo "PROGRESS: reinstalling a fresh version of mariadb 10"
sudo aptitude install -y mariadb-server-10.0

echo "PROGRESS: verifying that mysql is running"
mysql -e "show variables where Variable_name = 'datadir';"
mysql -e "show variables where Variable_name = 'tmpdir';"

